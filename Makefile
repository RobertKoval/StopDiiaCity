build:
	mkdir -p functions
	go get ./...
	go build -o functions/verify ./netlify-functions/...

chrome-extension-zip:
	cd ./public/extensions \
		&& rm -rf stopdiiacity-chrome-extension.zip \
		&& zip -r stopdiiacity-chrome-extension.zip stopdiiacity-chrome-extension \
		&& chmod 777 stopdiiacity-chrome-extension.zip

firefox-extension-zip:
	cd ./public/extensions \
		&& rm -rf stopdiiacity-firefox-extension.zip \
		&& zip -r stopdiiacity-firefox-extension.zip stopdiiacity-firefox-extension \
		&& chmod 777 stopdiiacity-firefox-extension.zip

template-generate:
	qtc -dir=templates -skipLineComments
	git add .

generate-html: template-generate
	go run ./cmd/main.go
	git add .