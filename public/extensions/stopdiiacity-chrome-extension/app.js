// chrome://extensions

console.log("StopDiiaCity run Chrome extension");

function verify(urls) {
    if (urls.length === 0) {
        return;
    }

    fetch("https://stopdiiacity.u8hub.com/verify.json", {
        method: "POST",
        body: JSON.stringify({
            "urls": urls,
        }),
    }).then(function (response) {
        return response.json();
    }).then(function (json) {
        if (json.exists) {
            const decoder = document.createElement("div")

            decoder.innerHTML = `<div id="js-modal-background" class="stopdiiacity-modal">
    <div class="stopdiiacity-modal-content">
        <div class="stopdiiacity-modal-header">
            <button id="js-modal-close" class="stopdiiacity-close" type="button">&times;</button>
            <h2>Обережно ймовірно компанія підтримує Дія City</h2>
        </div>
        <div class="stopdiiacity-modal-body">
            <h3>Списки компаній які підтримують Дія City</h3>
            <ul>
                <li><a href="https://dou.ua/forums/topic/33179/" target="_blank">Список на DOU.ua</a></li>
                <li><a href="https://web.archive.org/web/20210331162333/https://dc.diia.gov.ua/" target="_blank">Список логотипів за 21 березня 2021 року</a></li>
                <li><a href="https://web.archive.org/web/20210510104501/https://city.diia.gov.ua/" target="_blank">Список за 10 травня 2021 року</a></li>
            </ul>
            <h3>Петиція Стоп Дія Сіті</h3>
            <ul>
                <li><a href="https://petition.president.gov.ua/petition/114468" target="_blank">Петиція на сайті petition.president.gov.ua</a> (вже набрала 25465 голосів підтримки)</li>
                <li><a href="https://dou.ua/forums/topic/33040/" target="_blank">Обговорення на DOU.ua</a></li>
            </ul>
            <h3>Опитування проти Дія Сіті</h3>
            <ul>
                <li><a href="https://dou.ua/lenta/articles/what-devs-think-about-diia-city/" target="_blank">80% ІТ-спеціалістів проти. Результати опитування щодо Дія City</a></li>
                <li><a href="https://ain.ua/2021/02/17/opituvannya-xarkivskogo-it-klasteru-pro-diia-city/" target="_blank">50% компаній виведуть бізнес з України — опитування Харківського ІТ-кластеру про «Дія.City»</a></li>
            </ul>
            <h3>Варість аудиту Дія Сіті</h3>
            <ul>
                <li><a href="https://dou.ua/forums/topic/33668/" target="_blank">До $100k за два аудита</a></li>
            </ul>
        </div>
        <div class="stopdiiacity-modal-footer">
            <p>Made with <a href="https://www.itguild.org.ua/" style="color:white;" target="_blank">itguild.org.ua</a></p>
            <p><a href="https://dou.ua/forums/topic/33673/" style="color:white;" target="_blank">StopDiiaCity розширення для сайтів пошуку роботи</a></p>
        </div>
    </div>
</div>`;

            const $modal = decoder.firstChild;
            document.body.prepend($modal);
            document.getElementById("js-modal-close").addEventListener("click", function () {
                $modal.style.visibility = "hidden";
            });
            const $background = document.getElementById("js-modal-background");
            $background.addEventListener("click", function (event) {
                if (event.target === $background) {
                    $modal.style.visibility = "hidden";
                }
            });
        }
    }).catch(console.error);
}

{
    const url = window.location.href;
    if (url.startsWith("https://jobs.dou.ua/companies/")) {
        verify([url]);
    } else if (url.startsWith("https://djinni.co/jobs/company-")) {
        verify([url]);
    } else if (
        url === "https://djinni.co/jobs/"
        || url.startsWith("https://djinni.co/jobs/?")
        || url.startsWith("https://djinni.co/jobs/location-")
        || url.startsWith("https://djinni.co/jobs/keyword-")
        || url.startsWith("https://djinni.co/jobs/l-")
    ) {
        // NOP is main page
    } else if (url.startsWith("https://djinni.co/jobs/")) {
        // vacancy page
        const urls = [];
        document.querySelectorAll(".list-jobs__details__info a").forEach(function ($element) {
            urls.push($element.href);
        });
        verify(urls);
    } else if (url.startsWith("https://www.linkedin.com/company/")) {
        verify([url]);
    }
}
