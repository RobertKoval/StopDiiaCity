package verify

import (
	"encoding/json"
	"net/http"
	"strings"
)

const (
	// language=JSON
	unsafeMessage = `{"exists":true}`
	// language=JSON
	safeMessage = `{"exists":false}`
)

type Request struct {
	URL     string   `json:"url"`
	URLs    []string `json:"urls"`
	Version int      `json:"version"`
}

type PrefixGroup struct {
	Prefix   string
	Prefixes []string
}

type Response struct {
	Body       string
	StatusCode int
}

var stopDiiaCityPrefixes = []PrefixGroup{
	{
		Prefix: "https://jobs.dou.ua/companies/",
		Prefixes: []string{
			"https://jobs.dou.ua/companies/allright/",
			"https://jobs.dou.ua/companies/englishdom/",
			"https://jobs.dou.ua/companies/powercodelab/",
			"https://jobs.dou.ua/companies/powercode-academy/",
			"https://jobs.dou.ua/companies/genesis-technology-partners/",
			"https://jobs.dou.ua/companies/solid/",                 // Genesis
			"https://jobs.dou.ua/companies/headway-1/",             // Genesis
			"https://jobs.dou.ua/companies/socialtech/",            // Genesis
			"https://jobs.dou.ua/companies/obrio/",                 // Genesis
			"https://jobs.dou.ua/companies/boosters-product-team/", // Genesis
			"https://jobs.dou.ua/companies/betterme/",              // Genesis
			"https://jobs.dou.ua/companies/lift-stories-editor/",   // Genesis
			"https://jobs.dou.ua/companies/jooble/",
			"https://jobs.dou.ua/companies/netpeak/",
			"https://jobs.dou.ua/companies/leverx-group/",
			"https://jobs.dou.ua/companies/reface/",
			"https://jobs.dou.ua/companies/s-pro/",
			"https://jobs.dou.ua/companies/banza/",
			"https://jobs.dou.ua/companies/datagroup/",
			"https://jobs.dou.ua/companies/deus-robots/",
			"https://jobs.dou.ua/companies/parimatch-tech/",
			"https://jobs.dou.ua/companies/pokermatch/",
			"https://jobs.dou.ua/companies/epam-systems/",
			"https://jobs.dou.ua/companies/epam-anywhere/",
			"https://jobs.dou.ua/companies/ajax-systems/",
			"https://jobs.dou.ua/companies/kyivstar/",
			"https://jobs.dou.ua/companies/sigma-software/",
			"https://jobs.dou.ua/companies/ideasoft-io/",
			"https://jobs.dou.ua/companies/intetics-co/",
			"https://jobs.dou.ua/companies/playson/",
			"https://jobs.dou.ua/companies/smartiway/",
			"https://jobs.dou.ua/companies/ciklum/",
			"https://jobs.dou.ua/companies/nix-solutions-ltd/",
			"https://jobs.dou.ua/companies/softserve/",
			"https://jobs.dou.ua/companies/raiffeisen/",
			"https://jobs.dou.ua/companies/raiffeisen-bank-international-ag/",
			"https://jobs.dou.ua/companies/eleks/",
			"https://jobs.dou.ua/companies/petcube-inc/",
			"https://jobs.dou.ua/companies/softteco/",
			"https://jobs.dou.ua/companies/luxoft/",          // https://dou.ua/lenta/articles/industry-about-diia-city/
			"https://jobs.dou.ua/companies/luxoft-training/", // https://dou.ua/lenta/articles/industry-about-diia-city/
			"https://jobs.dou.ua/companies/adtelligent/",
			"https://jobs.dou.ua/companies/intersog/",
			"https://jobs.dou.ua/companies/gismart_com/",
			"https://jobs.dou.ua/companies/softpositive/",
			"https://jobs.dou.ua/companies/vodafone-ukraine/",
			"https://jobs.dou.ua/companies/plvision/",
			"https://jobs.dou.ua/companies/treeum/",
			"https://jobs.dou.ua/companies/globallogic/",
			"https://jobs.dou.ua/companies/graintrack/", // inside
		},
	},
	{
		Prefix: "https://djinni.co/jobs/",
		Prefixes: []string{
			"https://djinni.co/jobs/company-allright-cc765/",
			"https://djinni.co/jobs/company-englishdom-209e8/",
			"https://djinni.co/jobs/company-powercode-9f88a/",
			"https://djinni.co/jobs/company-genesis-bbc83/",
			"https://djinni.co/jobs/company-gen-tech-f1f4f/",
			"https://djinni.co/jobs/company-gen-tech2-427ee/",
			"https://djinni.co/jobs/company-genesis-tech-b88e2/",
			"https://djinni.co/jobs/company-amomedia-4c317/",
			"https://djinni.co/jobs/company-headway-app-81bee/",
			"https://djinni.co/jobs/company-solid-fintech-company-87d5d/",
			"https://djinni.co/jobs/company-nebula-project-by-genesis-ed113/",
			"https://djinni.co/jobs/company-socialtech-6b80f/",
			"https://djinni.co/jobs/company-genesis-holywater--6c47c/",
			"https://djinni.co/jobs/company-genesis-apps-fintech-58625/",
			"https://djinni.co/jobs/?keywords=Genesis",
			"https://djinni.co/jobs/company-betterme-4372c/",
			"https://djinni.co/jobs/company-jooble-e95dd/",
			"https://djinni.co/jobs/company-netpeak-group-20216/",
			"https://djinni.co/jobs/company-leverx-com-47815/",
			"https://djinni.co/jobs/company-refaceai-7538f/",
			"https://djinni.co/jobs/company-s-pro-4cd02/",
			"https://djinni.co/jobs/company-banza-f738b/",
			"https://djinni.co/jobs/company-datagroup-a26a2/",
			"https://djinni.co/jobs/company-deusrobots-com-46e96/",
			"https://djinni.co/jobs/company-parimatch-tech-b6a34/",
			"https://djinni.co/jobs/company-parimatch-international-1b06c/",
			"https://djinni.co/jobs/company-pokermatch-com-43742/",
			"https://djinni.co/jobs/company-epam-systems-bb0df/",
			"https://djinni.co/jobs/company-ajax-systems-8b02d/",
			"https://djinni.co/jobs/company-kyivstar-c5f1a/",
			"https://djinni.co/jobs/company-sigma-software-c03a7/",
			"https://djinni.co/jobs/company-ideasoft-dbe69/",
			"https://djinni.co/jobs/company-intetics-5221d/",
			"https://djinni.co/jobs/company-intetics-minsk-cb2dc/",
			"https://djinni.co/jobs/company-playson-05bc8/",
			"https://djinni.co/jobs/company-smartiway-6d23c/",
			"https://djinni.co/jobs/company-ciklum-international-80662/",
			"https://djinni.co/jobs/company-nix-solutions-fe08e/",
			"https://djinni.co/jobs/company-softserve-6bee7/",
			"https://djinni.co/jobs/company-softserve-dnipro-58f42/",
			"https://djinni.co/jobs/company-softserve-lviv-0de17/",
			"https://djinni.co/jobs/company-softserve-kharkiv-9b88e/",
			"https://djinni.co/jobs/company-raiffeisen-bank-aval-7b988/",
			"https://djinni.co/jobs/company-raiffeisen-bank-international-4cb67/",
			"https://djinni.co/jobs/company-eleks-6227d/",
			"https://djinni.co/jobs/company-petcube-com-a1c55/",
			"https://djinni.co/jobs/company-softteco-c9269/",
			"https://djinni.co/jobs/company-luxoft-ec4fe/",
			"https://djinni.co/jobs/company-adtelligent-751ce/",
			"https://djinni.co/jobs/company-intersog-fa680/",
			"https://djinni.co/jobs/company-gismart-e0be5/",
			"https://djinni.co/jobs/company-softpositive-37a51/",
			"https://djinni.co/jobs/company-vodafone-ukraine-85a78/",
			"https://djinni.co/jobs/company-plvision-a0d4d/",
			"https://djinni.co/jobs/company-treeum-6d9c0/",
			"https://djinni.co/jobs/company-globallogic-43eee/",
			"https://djinni.co/jobs/company-graintrack-cd726/",
		},
	},
	{
		Prefix: "https://www.linkedin.com/company/",
		Prefixes: []string{
			"https://www.linkedin.com/company/allright-com/",
			"https://www.linkedin.com/company/englishdom/",
			"https://www.linkedin.com/company/powercodecouk/",
			"https://www.linkedin.com/company/powercodeacademy/",

			"https://www.linkedin.com/company/genesis-technology-partners/",
			"https://www.linkedin.com/company/genesis-investments-vc/",  // Genesis Investments
			"https://www.linkedin.com/company/gthw-app-limited/",        // Genesis Headway
			"https://www.linkedin.com/company/amomedia-company/",        // Genesis AmoMedia
			"https://www.linkedin.com/company/obrio-genesis/",           // Genesis Obrio
			"https://www.linkedin.com/company/solidgate-technologies/",  // Genesis Solid
			"https://www.linkedin.com/company/socialtechnologies/",      // Genesis
			"https://www.linkedin.com/company/boosters-apps/",           // Genesis
			"https://www.linkedin.com/company/betterme-apps/",           // Genesis
			"https://www.linkedin.com/company/amomama-media-publisher/", // Genesis
			"https://www.linkedin.com/company/jooble/",
			"https://www.linkedin.com/company/netpeak-group/",
			"https://www.linkedin.com/company/netpeak/",
			"https://www.linkedin.com/company/leverx/",
			"https://www.linkedin.com/company/leverx-group/",
			"https://www.linkedin.com/company/refaceapp/",
			"https://www.linkedin.com/company/s-pro/",
			"https://www.linkedin.com/company/banzait/",
			"https://www.linkedin.com/company/datagroup1/",
			"https://www.linkedin.com/company/deus-robots/",

			"https://www.linkedin.com/company/parimatch-tech/",
			"https://www.linkedin.com/company/parimatch-global/",
			"https://www.linkedin.com/company/parimatch-international/",
			"https://www.linkedin.com/company/parimatch-cy/",
			"https://www.linkedin.com/company/parimatch-ukraine/",
			"https://www.linkedin.com/company/parimatch-belarus/",
			"https://www.linkedin.com/company/parimatch-africa/",
			"https://www.linkedin.com/company/parimatch-kazakhstan/",
			"https://www.linkedin.com/company/parimatch-russia/",
			"https://www.linkedin.com/company/pokermatch/",
			"https://www.linkedin.com/company/pokermatch-ukraine/",

			"https://www.linkedin.com/company/epam-systems/",
			"https://www.linkedin.com/company/ajax-systems/",
			"https://www.linkedin.com/company/kyivstar/",
			"https://www.linkedin.com/company/kyivstar-business-hub/",
			"https://www.linkedin.com/company/nika-tech-family/",
			"https://www.linkedin.com/company/beeventures/",
			"https://www.linkedin.com/company/sigma-software-group/",
			"https://www.linkedin.com/company/ideasoft.io/",
			"https://www.linkedin.com/company/intetics/",
			"https://www.linkedin.com/company/intetics-team/",
			"https://www.linkedin.com/company/playson/",
			"https://www.linkedin.com/company/zorachka/",
			"https://www.linkedin.com/company/digitalfuture/",
			"https://www.linkedin.com/company/smartiway/",
			"https://www.linkedin.com/company/ciklum/",
			"https://www.linkedin.com/company/nix-solutions-ltd/",
			"https://www.linkedin.com/company/softserve/",
			"https://www.linkedin.com/company/raiffeisen-ua/",
			"https://www.linkedin.com/company/eleks/",
			"https://www.linkedin.com/company/petcube/",
			"https://www.linkedin.com/company/softteco/",
			"https://www.linkedin.com/company/luxoft/",
			"https://www.linkedin.com/company/adtelligent/",
			"https://www.linkedin.com/company/intersog/",
			"https://www.linkedin.com/company/gismart/",
			"https://www.linkedin.com/company/softpositive/",
			"https://www.linkedin.com/company/vodafone-ukraine/",
			"https://www.linkedin.com/company/vodafone-retail-ukraine/",
			"https://www.linkedin.com/company/plvision/",
			"https://www.linkedin.com/company/treeum/",
			"https://www.linkedin.com/company/globallogic/",
			"https://www.linkedin.com/company/globallogicukraine/",
			"https://www.linkedin.com/company/graintrack/",
		},
	},
}

func Headers() map[string]string {
	return map[string]string{
		"Access-Control-Allow-Origin": "*",
		"Content-Type":                "application/json",
	}
}

func Verify(content []byte) Response {
	var request Request

	var unmarhsalErr = json.Unmarshal(content, &request)
	if unmarhsalErr != nil {
		return Response{`{}`, http.StatusBadRequest}
	}

	var urls = request.URLs
	if request.URL != "" {
		urls = append(urls, request.URL)
	}

	if len(urls) == 0 {
		return Response{`{}`, http.StatusBadRequest}
	}

	for _, prefixGroup := range stopDiiaCityPrefixes {
		for _, requestURL := range urls {
			if strings.HasPrefix(requestURL, prefixGroup.Prefix) {
				if hasAnyPrefix(prefixGroup.Prefixes, requestURL) {
					return Response{unsafeMessage, http.StatusOK}
				}
			}
		}
	}

	return Response{safeMessage, http.StatusOK}
}

func Prefixes() []PrefixGroup {
	return stopDiiaCityPrefixes
}

func hasAnyPrefix(prefixes []string, url string) bool {
	for _, prefix := range prefixes {
		if strings.HasPrefix(url, prefix) {
			return true
		}
	}

	return false
}
